//
//  ViewController.swift
//  Swift Error Factory
//
//  Created by Brendan Berkley on 3/2/21.
//  Copyright © 2021 Brendan Berkley. All rights reserved.
//

import UIKit
import Sentry

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        print("Hmmmm")
        SentrySDK.capture(message: "Hello, GlitchTip!")
    }


}

